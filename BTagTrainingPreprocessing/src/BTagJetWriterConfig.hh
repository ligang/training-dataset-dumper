#ifndef BTAG_JET_WRITER_CONFIG_HH
#define BTAG_JET_WRITER_CONFIG_HH

#include <map>
#include <string>
#include <vector>

struct BTagVariableMaps {
  std::map<std::string, std::string> replace_with_defaults_checks;
  std::map<std::string, std::string> rename;
};

struct BTagJetWriterConfig {
  BTagJetWriterConfig();

  std::string name;

  bool write_event_info;
  bool write_kinematics_relative_to_parent;
  bool write_substructure_moments;

  BTagVariableMaps variable_maps;

  std::vector<std::string> jet_int_variables;
  std::vector<std::string> jet_float_variables;

  std::vector<std::string> char_variables;
  std::vector<std::string> int_variables;
  std::vector<std::string> int_as_float_variables;
  std::vector<std::string> float_variables;
  std::vector<std::string> double_variables;

  std::vector<std::string> parent_jet_int_variables;
};


#endif
