#ifndef BTAGGING_CONSOLIDATOR_HH
#define BTAGGING_CONSOLIDATOR_HH

#include "xAODJet/JetContainer.h"

#include <string>
#include <map>
#include <regex>

class BTaggingConsolidator
{
public:
  using JC = xAOD::JetContainer;
  typedef std::vector<std::pair<std::string, const JC*>> InJets;
  BTaggingConsolidator(
    const std::map<std::string,std::pair<std::regex,std::string>>& jc_regex,
    const std::vector<std::string>& vars_to_copy);
  void consolidate(const xAOD::JetContainer* targ, const InJets& src) const;
private:
  struct Decorators {
    SG::AuxElement::ConstAccessor<double> source;
    SG::AuxElement::Decorator<double> target;
  };
  std::map<std::string, std::vector<Decorators>> m_decorators;
};

#endif
